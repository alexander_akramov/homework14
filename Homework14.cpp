﻿#include <iostream>
#include <string>

int main()
{  
   
    std::cout << "Enter your nick, please." << std::endl;
    std::string PlayerNick;
    std::getline(std::cin, PlayerNick);
    std::cout << "Your nick has " << PlayerNick.length() << " symbols." << "\n";
    std::cout << "The First symbol in your nick is " << PlayerNick.front() << 
        ". And the last symbol is " << PlayerNick.back() << "." << std::endl;
    
    return 0;
}

